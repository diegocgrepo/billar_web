from django.conf.urls import url
from django.contrib import admin
from usuarios.views import login_user, logout

urlpatterns = [
    url(r'^', login_user, name="login"),
]