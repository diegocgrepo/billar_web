from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from manager.models import Mesa, Venta, Orden
from manager.views import panel

def login_user(request):
    if request.user.is_authenticated():
        return panel(request)
    if request.method == "GET":
        return render(
            request,
            'login.html'
        )
    else:
        user = request.POST['user']
        password = request.POST['password']
        user = authenticate(username=user, password=password)
        if user is not None:
            login(request, user)
            return panel(request)
        else:
            return render(
                request,
                'login.html'
            )

def logout_user(request):
    logout(request)
    return redirect('/')
        
