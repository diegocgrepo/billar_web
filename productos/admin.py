from django.contrib import admin
from productos.models import Producto, EntradaProducto, Categoria
import decimal

@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('referencia', 'nombre', 'precio', 'precio_sin_iva', 'cantidad_en_inventario', 'iva',)
    search_fields = ['referencia', 'nombre']
    exclude = ('precio_sin_iva',)

    def save_model(self, request, obj, form, change):
        iva = decimal.Decimal("1.{}".format(obj.iva))
        iva = obj.precio - (obj.precio/iva)
        obj.precio_sin_iva = obj.precio - iva
        obj.save()

@admin.register(EntradaProducto)
class EntradaProductoAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'producto', 'cantidad_ingresada', 'usuario',)
    search_fields = ['producto']
    list_filter = ('usuario', 'fecha',)
    fields = ('producto', 'cantidad_ingresada',)

    def save_model(self, request, obj, form, change):
        obj.usuario = request.user
        obj.save()
        obj.producto.cantidad_en_inventario = obj.producto.cantidad_en_inventario + obj.cantidad_ingresada
        obj.producto.save()

@admin.register(Categoria)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
