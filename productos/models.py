from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import intcomma

class Categoria(models.Model):
    nombre = models.CharField(max_length=30)

    def __str__(self):
        return str(self.nombre)
    
    def obtener_ventas_dia(self, piso, raw=None):
        from manager.models import Venta
        ayer = datetime.date.today() - datetime.timedelta(days=1)
        ventas_mesa_hoy = Venta.objects.filter(terminada=True, fecha_fin__gt=ayer, mesa__piso=piso)
        total = 0
        for venta in ventas_mesa_hoy:
            ordenes_categoria = venta.related_ordenes.all().filter(producto__categoria=self)
            for orden in ordenes_categoria:
                total = total + orden.total
        if not raw:
            return intcomma(total)
        else: 
            return total
    
    def obtener_ventas_mes(self, piso, raw=None):
        from manager.models import Venta
        hoy = datetime.date.today()
        mes = hoy.month
        ventas_mesa_hoy = Venta.objects.filter(terminada=True, fecha_fin__month=mes, mesa__piso=piso)
        total = 0
        for venta in ventas_mesa_hoy:
            ordenes_categoria = venta.related_ordenes.all().filter(producto__categoria=self)
            for orden in ordenes_categoria:
                total = total + orden.total
        if not raw:
            return intcomma(total)
        else: 
            return total
        
class Producto(models.Model):
    referencia = models.CharField(max_length=200, null=True)
    nombre = models.CharField(max_length=200)
    precio = models.PositiveIntegerField(default=0)
    precio_sin_iva = models.PositiveIntegerField(default=0)
    cantidad_en_inventario = models.PositiveIntegerField(default=0)
    iva = models.PositiveIntegerField(null=True, default=16)
    categoria = models.ForeignKey(Categoria, null=True)

    def __str__(self):
        return str(self.nombre)
    
    def short_name(self):
        return str(nombre)

class EntradaProducto(models.Model):
    fecha = models.DateTimeField(auto_now_add=True)
    producto = models.ForeignKey(Producto)
    cantidad_ingresada = models.PositiveIntegerField()
    usuario = models.ForeignKey(User, null=True)

    def __str__(self):
        return str(self.producto.nombre)

