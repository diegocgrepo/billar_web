import datetime
from django.utils import timezone
from django.contrib import admin
from django.utils import formats
from django.http import HttpResponse
from manager.models import Mesa, Venta, Orden, Servicio, Movimiento
from productos.models import Producto, Categoria
from django.contrib.humanize.templatetags.humanize import intcomma


@admin.register(Mesa)
class MesaAdmin(admin.ModelAdmin):
    list_display = ('numero', 'tipo', 'piso', 'ip', 'mac')

@admin.register(Movimiento)
class MovimientoAdmin(admin.ModelAdmin):
    list_display = ('venta', 'fecha', 'producto', 'cantidad', 'tipo_accion', 'usuario')
    list_filter = ('usuario', 'fecha')

def reporte_categorias_diario_piso_1(modeladmin, request, queryset):
    from io import BytesIO
    from django.http import HttpResponse
    from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
    from reportlab.lib.styles import getSampleStyleSheet
    from reportlab.lib import colors
    from reportlab.lib.pagesizes import letter
    from reportlab.platypus import Table
    from django.contrib.humanize.templatetags.humanize import intcomma

    
    response = HttpResponse(content_type='application/pdf')
    pdf_name = "ventas.pdf"  
    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    ventas = []
    styles = getSampleStyleSheet()
    header = Paragraph("Listado de ventas por categoria del piso 1: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
    ventas.append(header)
    categorias_query = Categoria.objects.all().order_by('nombre')
    headings = ('Nombre', 'Total')
    categorias = [(c.nombre, c.obtener_ventas_dia(piso=1)) for c in categorias_query]

    total_categorias = []
    total_sum = 0

    for category in categorias_query:
        total_sum = total_sum + category.obtener_ventas_dia(piso=1, raw=True)
        total_categorias.append(intcomma(total_sum))
    
    total_row_header = ('Total del dia: {}'.format(intcomma(total_sum)),)

    t = Table([headings] + categorias + [total_row_header])

    t.setStyle(TableStyle(
        [
            ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
            ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
            ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
        ]
    ))
    ventas.append(t)
    doc.build(ventas)
    response.write(buff.getvalue())
    buff.close()
    return response

def reporte_categorias_diario_piso_2(modeladmin, request, queryset):
    from io import BytesIO
    from django.http import HttpResponse
    from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
    from reportlab.lib.styles import getSampleStyleSheet
    from reportlab.lib import colors
    from reportlab.lib.pagesizes import letter
    from reportlab.platypus import Table
    
    response = HttpResponse(content_type='application/pdf')
    pdf_name = "ventas.pdf"  
    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    ventas = []
    styles = getSampleStyleSheet()
    header = Paragraph("Listado de ventas por categoria del piso 2: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
    ventas.append(header)
    categorias_query = Categoria.objects.all().order_by('nombre')
    headings = ('Nombre', 'Total')
    categorias = [(c.nombre, c.obtener_ventas_dia(piso=2)) for c in categorias_query]

    total_categorias = []
    total_sum = 0

    for category in categorias_query:
        total_sum = total_sum + category.obtener_ventas_dia(piso=2, raw=True)
        total_categorias.append(intcomma(total_sum))
    
    total_row_header = ('Total del dia: {}'.format(intcomma(total_sum)),)

    t = Table([headings] + categorias + [total_row_header])

    t.setStyle(TableStyle(
        [
            ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
            ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
            ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
        ]
    ))
    ventas.append(t)
    doc.build(ventas)
    response.write(buff.getvalue())
    buff.close()
    return response

def reporte_categorias_mensual_piso_1(modeladmin, request, queryset):
    from io import BytesIO
    from django.http import HttpResponse
    from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
    from reportlab.lib.styles import getSampleStyleSheet
    from reportlab.lib import colors
    from reportlab.lib.pagesizes import letter
    from reportlab.platypus import Table
    
    response = HttpResponse(content_type='application/pdf')
    pdf_name = "ventas.pdf"  
    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    ventas = []
    styles = getSampleStyleSheet()
    header = Paragraph("Listado de ventas por categoria del piso 1: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
    ventas.append(header)
    categorias_query = Categoria.objects.all().order_by('nombre')
    headings = ('Nombre', 'Total')
    categorias = [(c.nombre, c.obtener_ventas_mes(piso=1)) for c in categorias_query]

    total_categorias = []
    total_sum = 0

    for category in categorias_query:
        total_sum = total_sum + category.obtener_ventas_mes(piso=1, raw=True)
        total_categorias.append(intcomma(total_sum))
    
    total_row_header = ('Total del mes: {}'.format(intcomma(total_sum)),)

    t = Table([headings] + categorias + [total_row_header])

    t.setStyle(TableStyle(
        [
            ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
            ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
            ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
        ]
    ))
    ventas.append(t)
    doc.build(ventas)
    response.write(buff.getvalue())
    buff.close()
    return response

def reporte_categorias_mensual_piso_2(modeladmin, request, queryset):
    from io import BytesIO
    from django.http import HttpResponse
    from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
    from reportlab.lib.styles import getSampleStyleSheet
    from reportlab.lib import colors
    from reportlab.lib.pagesizes import letter
    from reportlab.platypus import Table

    response = HttpResponse(content_type='application/pdf')
    pdf_name = "ventas.pdf"  
    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    ventas = []
    styles = getSampleStyleSheet()
    header = Paragraph("Listado de ventas por categoria del: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
    ventas.append(header)
    categorias_query = Categoria.objects.all().order_by('nombre')
    headings = ('Nombre', 'Total')
    categorias = [(c.nombre, c.obtener_ventas_mes(piso=2)) for c in categorias_query]

    total_categorias = []
    total_sum = 0

    for category in categorias_query:
        total_sum = total_sum + category.obtener_ventas_mes(piso=2, raw=True)
        total_categorias.append(intcomma(total_sum))
    
    total_row_header = ('Total del mes: {}'.format(intcomma(total_sum)),)

    t = Table([headings] + categorias + [total_row_header])

    t.setStyle(TableStyle(
        [
            ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
            ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
            ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
        ]
    ))
    ventas.append(t)
    doc.build(ventas)
    response.write(buff.getvalue())
    buff.close()
    return response

# def reporte_juego_diario(modeladmin, request, queryset):
#     from io import BytesIO
#     from django.http import HttpResponse
#     from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
#     from reportlab.lib.styles import getSampleStyleSheet
#     from reportlab.lib import colors
#     from reportlab.lib.pagesizes import letter
#     from reportlab.platypus import Table

#     response = HttpResponse(content_type='application/pdf')
#     pdf_name = "ventas.pdf" 

#     buff = BytesIO()
#     doc = SimpleDocTemplate(buff,
#                             pagesize=letter,
#                             rightMargin=40,
#                             leftMargin=40,
#                             topMargin=60,
#                             bottomMargin=18,
#                             )
#     ventas = []
#     styles = getSampleStyleSheet()
#     header = Paragraph("Listado de consumo de juego del billar por mesas del: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
#     ventas.append(header)
#     mesas_query = Mesa.objects.filter(tipo="NORMAL").order_by('numero')
#     headings = ('Numero de mesa', 'Total')
#     mesas = [(m.numero, m.obtener_juego_dia()) for m in mesas_query]

#     total_juego = []
#     total_sum = 0

#     for m in mesas_query:
#         total_sum = total_sum + m.obtener_juego_dia()
#         total_juego.append(intcomma(total_sum))
    
#     total_row_header = ('Total del diario: {}'.format(intcomma(total_sum)),)

#     t = Table([headings] + mesas + [total_row_header])
#     t.setStyle(TableStyle(
#         [
#             ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
#             ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
#             ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
#         ]
#     ))
#     ventas.append(t)
#     doc.build(ventas)
#     response.write(buff.getvalue())
#     buff.close()
#     return response

# def reporte_juego_diario(modeladmin, request, queryset):
#     from io import BytesIO
#     from django.http import HttpResponse
#     from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
#     from reportlab.lib.styles import getSampleStyleSheet
#     from reportlab.lib import colors
#     from reportlab.lib.pagesizes import letter
#     from reportlab.platypus import Table

#     response = HttpResponse(content_type='application/pdf')
#     pdf_name = "ventas.pdf" 

#     buff = BytesIO()
#     doc = SimpleDocTemplate(buff,
#                             pagesize=letter,
#                             rightMargin=40,
#                             leftMargin=40,
#                             topMargin=60,
#                             bottomMargin=18,
#                             )
#     ventas = []
#     styles = getSampleStyleSheet()
#     header = Paragraph("Listado de consumo de juego del billar por mesas del: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
#     ventas.append(header)
#     mesas_query = Mesa.objects.filter(tipo="NORMAL").order_by('numero')
#     headings = ('Numero de mesa', 'Total')
#     mesas = [(m.numero, m.obtener_juego_mes()) for m in mesas_query]

#     total_juego = []
#     total_sum = 0

#     for m in mesas_query:
#         total_sum = total_sum + m.obtener_juego_mes()
#         total_juego.append(intcomma(total_sum))
    
#     total_row_header = ('Total del mes: {}'.format(intcomma(total_sum)),)

#     t = Table([headings] + mesas + [total_row_header])
#     t.setStyle(TableStyle(
#         [
#             ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
#             ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
#             ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
#         ]
#     ))
#     ventas.append(t)
#     doc.build(ventas)
#     response.write(buff.getvalue())
#     buff.close()
#     return response

# def reporte_barras(modeladmin, request, queryset):
#     from io import BytesIO
#     from django.http import HttpResponse
#     from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
#     from reportlab.lib.styles import getSampleStyleSheet
#     from reportlab.lib import colors
#     from reportlab.lib.pagesizes import letter
#     from reportlab.platypus import Table

#     response = HttpResponse(content_type='application/pdf')
#     pdf_name = "ventas.pdf"  # llamado clientes
#     # la linea 26 es por si deseas descargar el pdf a tu computadora
#     # response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
#     buff = BytesIO()
#     doc = SimpleDocTemplate(buff,
#                             pagesize=letter,
#                             rightMargin=40,
#                             leftMargin=40,
#                             topMargin=60,
#                             bottomMargin=18,
#                             )
#     ventas = []
#     styles = getSampleStyleSheet()
#     header = Paragraph("Listado de ventas por barras del: " + str(formats.date_format(datetime.datetime.now(), "SHORT_DATETIME_FORMAT")), styles['Heading1'])
#     ventas.append(header)
#     mesas = Mesa.objects.filter(tipo="BARRA").order_by('numero')
#     headings = ('Numero', 'Total consumo')
#     mesas = [(m.numero, m.obtener_ventas_dia()) for m in mesas]
#     t = Table([headings] + mesas)
#     t.setStyle(TableStyle(
#         [
#             ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
#             ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
#             ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
#         ]
#     ))
#     ventas.append(t)
#     doc.build(ventas)
#     response.write(buff.getvalue())
#     buff.close()
#     return response

reporte_categorias_diario_piso_1.short_description = u"Imprimir informes por categoria diario PISO 1"
reporte_categorias_diario_piso_2.short_description = u"Imprimir informes por categoria diario PISO 2"
reporte_categorias_mensual_piso_1.short_description = u'Imprimir informes por categoria mensual PISO 1'
reporte_categorias_mensual_piso_2.short_description = u'Imprimir informes por categoria mensual PISO 2'

class OrdenInline(admin.TabularInline):
    model = Orden
    readonly_fields = ('venta', 'producto', 'cantidad', 'total',)
    extra = 0
    can_delete = False
    can_add = False


@admin.register(Venta)
class VentaAdmin(admin.ModelAdmin):
    list_display = ('mesa', 'total', 'fecha_inicio', 'fecha_fin', 'terminada', 'obtener_duracion')
    list_filter = ('mesa__numero', 'fecha_inicio', 'terminada')
    search_fields = ['pk']
    actions = [reporte_categorias_diario_piso_1, reporte_categorias_diario_piso_2, reporte_categorias_mensual_piso_1, reporte_categorias_mensual_piso_2]
    inlines = [OrdenInline,]

    class Media:
        js = ['/static/js/jquery.js', '/static/js/init.js']

@admin.register(Orden, Servicio)
class ManagerAdmin(admin.ModelAdmin):
    pass
