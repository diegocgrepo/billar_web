# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-19 22:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0018_movimiento_tipo_accion'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicio',
            name='horario',
            field=models.CharField(choices=[('DIA', 'DIA'), ('NOCHE', 'NOCHE')], default='DIA', max_length=40),
        ),
        migrations.AlterField(
            model_name='servicio',
            name='precio',
            field=models.PositiveIntegerField(help_text='Precio por minuto'),
        ),
    ]
