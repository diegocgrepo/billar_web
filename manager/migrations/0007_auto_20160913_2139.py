# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-14 02:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0006_auto_20160913_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='mesa',
            name='ip',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='mesa',
            name='mac',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
