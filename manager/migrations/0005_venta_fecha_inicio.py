# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-02 23:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0004_auto_20160902_2308'),
    ]

    operations = [
        migrations.AddField(
            model_name='venta',
            name='fecha_inicio',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
