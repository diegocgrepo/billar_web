# -*- encoding: utf-8 -*-
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic.detail import DetailView
from manager.models import Mesa, Venta, Orden, Servicio, Movimiento
from productos.models import Producto
import datetime
from django.utils import timezone

class VentaDetailView(DetailView):
    model = Venta
    template_name = 'venta.html'

    def get_context_data(self, **kwargs):
        context = super(VentaDetailView, self).get_context_data(**kwargs)
        context['productos'] = Producto.objects.filter(cantidad_en_inventario__gt=0)
        now = timezone.now() - self.object.fecha_inicio
        context['minutos_transcurridos'] = now.seconds / 60
        context['segundos_transcurridos'] = now.seconds
        hora_actual = timezone.localtime(timezone.now())
        try:
            if hora_actual.hour > 6 and hora_actual.hour < 14:
                billar = Servicio.objects.get(nombre="Billar", horario="DIA")
            else:
                billar = Servicio.objects.get(nombre="Billar", horario="NOCHE")
        except:
            messages.add_message(
                self.request,
                messages.SUCCESS,
                'El administrador no ha configurado los precios del billar.'
            )
            return redirect('/ventas/{}'.format(self.object.pk))
        context['valor_hora'] = billar.precio
        return context

def agregar_producto(request):
    try:
        id_producto = request.POST['id_producto']
        id_venta = request.POST['id_venta']
        cantidad = int(request.POST['cantidad'])
    except: 
        messages.add_message(
            request,
            messages.WARNING,
            'Olvidaste seleccionar la cantidad o el producto'
        )
        return redirect('/ventas/')


    producto = Producto.objects.get(pk=id_producto)
    venta = Venta.objects.get(pk=id_venta)

    if cantidad < 0:
        messages.add_message(
            request,
            messages.WARNING,
            'No puedes agregar cantidades negativas'
        )
        return redirect('/ventas/{}/'.format(venta.pk))
    
    if cantidad > producto.cantidad_en_inventario:
        messages.add_message(
            request,
            messages.WARNING,
            'Cantidad no disponible en inventario (Stock: {})'.format(producto.cantidad_en_inventario)
        )
        return redirect('/ventas/{}/'.format(venta.pk))
    else:
        producto.cantidad_en_inventario = producto.cantidad_en_inventario - cantidad
        producto.save()

    es_igual = False

    for item in venta.related_ordenes.all():
        if item.producto == producto:
            es_igual = True
            orden = item
            item.cantidad = item.cantidad + cantidad
            item.total = item.producto.precio*item.cantidad
            item.save()
    
    if not es_igual:
        subtotal = producto.precio * int(cantidad)
        orden = Orden(
            venta = venta,
            producto = producto,
            cantidad = cantidad,
            total = subtotal
        )
        orden.save()
    
    total = 0

    for item in venta.related_ordenes.all():
        total = total + item.total 
        venta.total = total
        venta.save()
    
    movimiento = Movimiento(
        venta = orden.venta,
        producto = orden.producto,
        usuario = request.user,
        cantidad = orden.cantidad,
        tipo_accion = 'Adición'
    )
    movimiento.save()

    messages.add_message(
        request,
        messages.INFO,
        '{} agregado correctamente'.format(producto.nombre)
    )
    return redirect('/ventas/{}/'.format(venta.pk))

def eliminar_orden(request):
    id_orden = request.GET['id_orden']
    orden = Orden.objects.get(pk=id_orden)
    orden.venta.total = orden.venta.total - orden.total
    orden.venta.save()
    movimiento = Movimiento(
        venta = orden.venta,
        producto = orden.producto,
        usuario = request.user,
        cantidad = orden.cantidad,
        tipo_accion = 'Eliminación'
    )
    movimiento.save()
    orden.producto.cantidad_en_inventario = orden.producto.cantidad_en_inventario + orden.cantidad
    orden.producto.save()
    orden.delete()
    messages.add_message(
        request,
        messages.SUCCESS,
        '{} eliminado'.format(orden.producto.nombre)
    )
    return redirect('/ventas/{}/'.format(orden.venta.pk))

def crear_venta(request):
    mesa = Mesa.objects.get(numero=request.POST['num_mesa'])
    venta = Venta(
        mesa = mesa
    )
    venta.save()
    return redirect('/ventas/{}/'.format(venta.pk))

def facturar_venta(request):
    id_venta = request.GET['id_venta']
    venta = Venta.objects.get(pk=id_venta)
    venta.fecha_fin = timezone.now()
    precio_tiempo = None
    minutos_transcurridos = None
    if venta.mesa.tipo == "NORMAL":
        hora_actual = timezone.localtime(timezone.now())
        if venta.mesa.piso == 2:
            valor_billar = 130
        else:
            try:
                if venta.fecha_inicio.hour > 6 and venta.fecha_inicio.hour < 14:
                    billar = Servicio.objects.get(nombre="Billar", horario="DIA")
                else:
                    billar = Servicio.objects.get(nombre="Billar", horario="NOCHE")
            except Exception as e:
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'El administrador no ha configurado los precios del billar. Codigo (%s)' % e
                )
                return redirect('/ventas/{}/'.format(venta.pk))
            valor_billar = billar.precio
        duracion = venta.fecha_fin - venta.fecha_inicio
        minutos_transcurridos = duracion.seconds / 60
        precio_tiempo = int(valor_billar) * int(minutos_transcurridos)
        venta.total = venta.total + precio_tiempo
    venta.terminada = True
    venta.save()
    rows = int(17)-int(len(venta.related_ordenes.all()))
    rows = range(rows)
    return render(
        request,
        'factura.html',
        {
            'venta': venta,
            'rows': rows,
            'precio_tiempo': precio_tiempo,
            'minutos_transcurridos': minutos_transcurridos
        }
    )

def panel(request):
    return render(
        request,
        'panel.html',
        {
            'mesas': Mesa.objects.filter(tipo="NORMAL").order_by('numero'),
            'barras': Mesa.objects.filter(tipo="BARRA").order_by('numero')
        }
    )