from django.conf.urls import url

from manager.views import VentaDetailView, panel, agregar_producto

urlpatterns = [
    url(r'^$', panel, name='panel'),
    url(r'^(?P<pk>[-\w]+)/$', VentaDetailView.as_view(), name='venta-detail'),
]