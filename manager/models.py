# -*- encoding: utf-8 -*-

from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import User
from productos.models import Producto
from django.contrib.humanize.templatetags.humanize import intcomma


class Servicio(models.Model):
    DIA = "DIA"
    NOCHE = "NOCHE"
    HORARIO_CHOICES = (
        (DIA, 'DIA'),
        (NOCHE, 'NOCHE'),
    )
    nombre = models.CharField(max_length=200, default="Billar")
    horario = models.CharField(max_length=40, default="DIA", choices=HORARIO_CHOICES)
    precio = models.PositiveIntegerField(help_text="Precio por minuto")

    def __str__(self):
        return str(self.nombre)

class Mesa(models.Model):
    NORMAL = "NORMAL"
    BARRA = "BARRA"
    TIPO_CHOICES = (
        (NORMAL, 'NORMAL'),
        (BARRA, 'BARRA'),
    )
    numero = models.PositiveIntegerField(unique=True)
    tipo = models.CharField(choices=TIPO_CHOICES, max_length=10, default=NORMAL)
    ip = models.CharField(null=True, blank=True, max_length=255)
    mac = models.CharField(null=True, blank=True, max_length=255)
    piso = models.PositiveIntegerField(default=1)

    def __str__(self):
        return str(self.numero)
    
    def obtener_estado(self):
        if len(self.related_ventas.all().filter(terminada=False)) > 0:
            return 'Ocupada'
        else:
            return 'Disponible'
    
    def obtener_venta(self):
        if len(self.related_ventas.all()) > 0:
            return self.related_ventas.all().filter(terminada=False).last()
        else:
            return None
    
    def obtener_ventas_dia(self, c=None):
        ayer = datetime.date.today() - datetime.timedelta(days=1)
        ventas_mesa_hoy = self.related_ventas.all().filter(terminada=True, fecha_fin__gt=ayer)
        total = 0
        if c is None:
            for venta in ventas_mesa_hoy:
                total = total + venta.total
        else:
            for venta in ventas_mesa_hoy:
                ordenes_categoria = venta.related_ordenes.all().filter(producto__categoria=c)
                for orden in ordenes_categoria:
                    total = total + orden.total
        return intcomma(total)
    
    # def obtener_juego_dia(self, c=None):
    #     ayer = datetime.date.today() - datetime.timedelta(days=1)
    #     ventas_mesa_hoy = self.related_ventas.all().filter(terminada=True, fecha_fin__gt=ayer)
    #     total = 0
    #     for venta in ventas_mesa_hoy:
    #         if venta.fecha_inicio.hour > 6 and venta.fecha_inicio.hour < 14:
    #             billar = Servicio.objects.get(nombre="Billar", horario="DIA")
    #         else:
    #             billar = Servicio.objects.get(nombre="Billar", horario="NOCHE")
    #         valor_billar = billar.precio
    #         duracion = venta.fecha_fin - venta.fecha_inicio
    #         minutos_transcurridos = duracion.seconds / 60
    #         precio_tiempo = int(valor_billar) * int(minutos_transcurridos)
    #         total = total + precio_tiempo
    #     return intcomma(total)
    
    # def obtener_juego_mes(self, c=None):
    #     hoy = datetime.date.today() 
    #     mes = hoy.month
    #     ventas_mesa_hoy = self.related_ventas.all().filter(terminada=True, fecha_fin__month=mes)
    #     total = 0
    #     for venta in ventas_mesa_hoy:
    #         if venta.fecha_inicio.hour > 6 and venta.fecha_inicio.hour < 14:
    #             billar = Servicio.objects.get(nombre="Billar", horario="DIA")
    #         else:
    #             billar = Servicio.objects.get(nombre="Billar", horario="NOCHE")
    #         valor_billar = billar.precio
    #         duracion = venta.fecha_fin - venta.fecha_inicio
    #         minutos_transcurridos = duracion.seconds / 60
    #         precio_tiempo = int(valor_billar) * int(minutos_transcurridos)
    #         total = total + precio_tiempo
    #     return intcomma(total)
 
class Venta(models.Model):
    mesa = models.ForeignKey(Mesa, related_name="related_ventas")
    total = models.PositiveIntegerField(default=0)
    fecha_inicio = models.DateTimeField(auto_now_add=True)
    fecha_fin = models.DateTimeField(null=True)
    terminada = models.BooleanField(default=False)

    def __str__(self):
        return str(self.pk)
    
    def obtener_duracion(self):
        if self.terminada is True:
            if self.mesa.tipo != "BARRA":
                duracion = self.fecha_fin - self.fecha_inicio
                minutos_transcurridos = duracion.seconds / 60
                return "%s minutos" % (minutos_transcurridos)
            else:
                return "Barra"
        else:
            return "No terminada aún"
        

class Orden(models.Model):
    venta = models.ForeignKey(Venta, related_name="related_ordenes")
    producto = models.ForeignKey(Producto)
    cantidad = models.PositiveIntegerField(null=True)
    total = models.PositiveIntegerField(default=0)


    class Meta:
        verbose_name = 'Orden'
        verbose_name_plural = 'Ordenes'

    def __str__(self):
        return str(self.pk)
    
    def producto_short_name(self):
        return self.producto.nombre

class Movimiento(models.Model):
    venta = models.ForeignKey(Venta, null=True)
    usuario = models.ForeignKey(User)
    producto = models.ForeignKey(Producto, null=True)
    fecha = models.DateTimeField(auto_now_add=True)
    cantidad = models.PositiveIntegerField(null=True)
    tipo_accion = models.CharField(max_length=30)

    def __str__(self):
        return str(self.usuario)